import React from 'react'
import { render } from 'react-dom'
import Alert from 'react-s-alert'

import Root from './containers/Root'
import configureStore from './store/configureStore'
import registerServiceWorker from './registerServiceWorker'
import './index.css'

const {store, history} = configureStore()

// Subscribe to the Redux store and update the page title when it changes
var previousPageTitle = store.getState().pageTitle
var newPageTitle = previousPageTitle
store.subscribe(() => {
  newPageTitle = store.getState().pageTitle
  if (previousPageTitle !== newPageTitle) {
    previousPageTitle = newPageTitle
    document.title = newPageTitle
  }
})

//Render root element
render(
  <Root store={store} history={history} />,
  document.getElementById('root')
)

//Send notification if we are running a development build
if (process.env.NODE_ENV === 'development') {
  let devMessage = (
    <div>
      <h4>Development Build</h4>
      <p>
        This is a develpment version of this app. You can acces Redux DevTools
        with <strong> {process.env.REACT_APP_DEV_DOCK_VISIBLE_SHORTCUT}</strong>
        . You can change the position of the dock with
        <strong> {process.env.REACT_APP_DEV_DOCK_POSITION_SHORTCUT}</strong>.
        You can switch between different monitors with
        <strong> {process.env.REACT_APP_DEV_DOCK_MONITOR_SHORTCUT}</strong>.
      </p>
    </div>
  )

  Alert.info(devMessage, {
    timeout: 12000,
    effect: 'bouncyflip',
    position: 'bottom-right'
  })
}

//Display the development tools popup if not in a production environment
//if (process.env.NODE_ENV !== 'production' {
//  const showDevTools = require('./showDevTools').default
//  showDevTools(store)
//}

// Use a service worker to manage loading and caching resources
registerServiceWorker()
