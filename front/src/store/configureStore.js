import { createStore, applyMiddleware } from 'redux'
import createHistory from 'history/createBrowserHistory'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import { persistState } from 'redux-devtools'

import rootReducer from '../reducers'
import saga from '../sagas'

// History object used for routing state
const history = createHistory()

const sagaMiddleware = createSagaMiddleware()
// The enhancers for the redux store
let enhancer = null
if (process.env.NODE_ENV === 'development') {
  // Read the ?debug_session=<key> variable from the url
  const DevTools = require('../containers/DevTools').default

  function getDebugSessionKey() {
    const matches = window.location.href.match(/[?&]debug_session=([^&#]+)\b/);
    return (matches && matches.length > 0)? matches[1] : null;
  }

  const redux = require('redux')

  enhancer = redux.compose(
    applyMiddleware(
      routerMiddleware(history),
      sagaMiddleware
    ),
    DevTools.instrument(),
    //Lets you write ?debug_session=<key> in address bar to persist debug sessions
    persistState(getDebugSessionKey())
  )
} else {
  enhancer = applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware
  )
}

// Configure the Redux store
export default function configureStore(initialState) {
  const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    enhancer
  )

  sagaMiddleware.run(saga)

  if (process.env.NODE_ENV === 'development') {
    // Hot reload reducers
    if (module.hot) {
      module.hot.accept('../reducers', () => {
        store.replaceReducer(require('../reducers').default)
      })
    }
  }

  return {store, history}
}
