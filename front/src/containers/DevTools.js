import React from 'react'
import { createDevTools } from 'redux-devtools'

import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'
import Inspector from 'redux-devtools-inspector'
import SliderMonitor from 'redux-slider-monitor'
import Dispatcher from 'redux-devtools-dispatch'
import MultipleMonitors from 'redux-devtools-multiple-monitors';

const actionCreators = {
  addIssue(text) {
    return {
      type: 'ADD_TODO',
      text
    }
  }
}

// The monitor is a docked log monitor
const DevTools = createDevTools(
  <DockMonitor toggleVisibilityKey={process.env.REACT_APP_DEV_DOCK_VISIBLE_SHORTCUT}
               changePositionKey={process.env.REACT_APP_DEV_DOCK_POSITION_SHORTCUT}
               changeMonitorKey={process.env.REACT_APP_DEV_DOCK_MONITOR_SHORTCUT}
               defaultIsVisible={false}>
    <MultipleMonitors>
      <LogMonitor theme="tomorrow" />
      <Dispatcher actionC reators={actionCreators} />
    </MultipleMonitors>
    <MultipleMonitors>
      <Inspector />
      <Dispatcher actionCreators={actionCreators} />
    </MultipleMonitors>
    <SliderMonitor />
  </DockMonitor>
)

export default DevTools
