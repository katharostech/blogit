import React from 'react'
import { Route } from 'react-router-dom'
import Alert from 'react-s-alert'
import { LinkContainer } from 'react-router-bootstrap'
import { Helmet } from 'react-helmet'
import { Navbar, Nav, NavItem, Grid, Row, Col } from 'react-bootstrap'

import WYSIWYG from '../components/WYSIWYG'
import BlogArticle from '../components/BlogArticle'

// Bootstrap styles
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

// Alert Styles
import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/slide.css'
import 'react-s-alert/dist/s-alert-css-effects/scale.css'
import 'react-s-alert/dist/s-alert-css-effects/bouncyflip.css'
import 'react-s-alert/dist/s-alert-css-effects/flip.css'
import 'react-s-alert/dist/s-alert-css-effects/genie.css'
import 'react-s-alert/dist/s-alert-css-effects/jelly.css'
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css'

import './App.css'

const App = () => (
  <div>
    <Helmet>
      <title>{process.env.REACT_APP_NAME}</title>
    </Helmet>
    <Alert stack={{limit: 3}}
           position='top'
           effect='stackslide'/>
    <Navbar collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer exact to='/'>
            <a>{process.env.REACT_APP_NAME}</a>
          </LinkContainer>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <LinkContainer to='/blog'>
            <NavItem eventKey={1}>My Blog</NavItem>
          </LinkContainer>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    <div id='main'>
      <Grid>
        <Row>
          <Col>
            <Route path='/blog' component={WYSIWYG} />
            <Route path='/test'
                   render={props => (
                     <BlogArticle title='Blogit Comes to Town'
                                  author='Zicklag'
                                  body='<strong>Read</strong> the title above. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
                                  comments={[
                                    {
                                      username: "Zicklag",
                                      body: "<strong>Hello </strong><em>World</em>"
                                    },
                                    {
                                      username: "Harsulin's Ghost",
                                      body: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                                    }
                                  ]} />
            )}/>
          </Col>
        </Row>
      </Grid>
    </div>
  </div>
)

export default App
