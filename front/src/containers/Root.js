import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter as Router } from 'connected-react-router'

import App from './App'

let devComponents = null
if(process.env.NODE_ENV === 'development') {
  var DevTools = require('./DevTools').default
  devComponents = <DevTools />
}

const Root = ({ store, history }) => (
  <Provider store={store}>
    <Router history={history} basename={process.env.REACT_APP_BASE_PATH}>
      <div>
        <App />
        {devComponents}
      </div>
    </Router>
  </Provider>
)

export default Root
