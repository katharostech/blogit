import React from 'react';
import { render } from 'react-dom';
import Root from './Root';

import configureStore from '../store/configureStore'

it('renders without crashing', () => {
  const {store, history} = configureStore()

  const div = document.createElement('div');
  render(<Root store={store} history={history} />, div);
});
