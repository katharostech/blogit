import React from 'react'
import ReactHTMLParser from 'react-html-parser'
import DOMPurify from 'dompurify'

const SanitizedHTML = ({html}) => (
  <div>
    {ReactHTMLParser(DOMPurify.sanitize(html))}
  </div>
)

export default SanitizedHTML
