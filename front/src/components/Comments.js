import React from 'react'
import PropTypes from 'prop-types'

import Comment from './Comment'

import './Comments.css'

const Comments = ({comments}) => (
  <div className="comments-section">
  <h2>Comments</h2>
    <div className="comments">
      {comments.map(comment =>
        <Comment body={comment.body} username={comment.username} />
      )}
    </div>
  </div>
)

Comments.propTypes = {
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired
    }).isRequired
  )
}

export default Comments
