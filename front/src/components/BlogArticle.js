import React from 'react'
import PropTypes from 'prop-types'
import { PageHeader } from 'react-bootstrap'

import SanitizedHTML from './SanitizedHTML'
import Comments from './Comments'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import './BlogArticle.css'

const BlogArticle = ({title, author, body, comments}) => (
  <div className="blog-article">
    <PageHeader className="blog-article--header">
      {title} <small>{author}</small>
    </PageHeader>
    <article className="blog-article--body">
      <SanitizedHTML html={body} />
    </article>
    <Comments comments={comments} />
  </div>
)

BlogArticle.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired
    }).isRequired
  )
}

export default BlogArticle
