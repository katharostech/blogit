import React from 'react'
import PropTypes from 'prop-types'
import { Media } from 'react-bootstrap'

import SanitizedHTML from './SanitizedHTML'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import './Comment.css'

const Comment = ({username, body}) => (
  <Media className="comment">
    <Media.Body>
      <Media.Heading>{username}</Media.Heading>
      <SanitizedHTML html={body} />
    </Media.Body>
  </Media>
)

Comment.propTypes = {
  username: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired
}

export default Comment
