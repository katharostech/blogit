import React, { Component } from 'react'
import CKEditor from 'react-ckeditor-wrapper'

export default class WYSIWYG extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
    }
  }

  updateContent(value) {
    this.setState({content: value})
  }

  render() {
    return (<CKEditor
    value={this.state.content}
    onChange={this.updateContent.bind(this)} />)
  }
}
